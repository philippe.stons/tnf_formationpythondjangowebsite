from django.db import models

# Create your models here.

class Artist(models.Model):
    name = models.CharField(max_length=200, unique=True)

    def __str__(self) -> str:
        return self.name

class Contact(models.Model):
    email = models.EmailField(max_length=100)
    lastname = models.CharField(max_length=200)
    firstname = models.CharField(max_length=200)

    def __str__(self) -> str:
        return self.firstname + " " + self.lastname

class Album(models.Model):
    reference = models.IntegerField(null=True)
    create_at = models.DateTimeField(auto_now_add=True)
    available = models.BooleanField(default=True)
    title = models.CharField(max_length=200)
    picture = models.URLField()
    artists = models.ManyToManyField(Artist, related_name="album", blank=True)

    def __str__(self) -> str:
        return self.title